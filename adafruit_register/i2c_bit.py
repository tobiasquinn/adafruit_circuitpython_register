# SPDX-FileCopyrightText: 2016 Scott Shawcroft for Adafruit Industries
#
# SPDX-License-Identifier: MIT
# pylint: disable=too-few-public-methods

"""
`adafruit_register.i2c_bit`
====================================================

Single bit registers

* Author(s): Scott Shawcroft
"""

__version__ = "0.0.0-auto.0"
__repo__ = "https://github.com/adafruit/Adafruit_CircuitPython_Register.git"


class RWBit:
    """
    Single bit register that is readable and writeable.

    Values are `bool`

    :param int register_address: The register address to read the bit from
    :param type bit: The bit index within the byte at ``register_address``
    :param int register_width: The number of bytes in the register. Defaults to 1.
    :param bool lsb_first: Is the first byte we read from I2C the LSB? Defaults to true

    """

    def __init__(self, register_address, bit, register_width=1, lsb_first=True):
        self.register_address = register_address
        self.register_width = register_width
        self.bit_mask = 1 << (bit % 8)  # the bitmask *within* the byte!
        if lsb_first:
            self.byte = bit // 8  # the byte number within the buffer
        else:
            # the byte number within the buffer
            self.byte = register_width - (bit // 8) - 1

    def __get__(self, obj, objtype=None):
        _buffer = bytearray(self.register_width)
        obj.i2c_device.readfrom_mem_into(obj.address, self.register_address, _buffer)
        return bool(_buffer[self.byte] & self.bit_mask)

    def __set__(self, obj, value):
        _buffer = bytearray(self.register_width)
        obj.i2c_device.readfrom_mem_into(obj.address, self.register_address, _buffer)
        if value:
            _buffer[self.byte-1] |= self.bit_mask
        else:
            _buffer[self.byte-1] &= ~self.bit_mask
        obj.i2c_device.writeto_mem(obj.address, self.register_address, _buffer)


class ROBit(RWBit):
    """Single bit register that is read only. Subclass of `RWBit`.

    Values are `bool`

    :param int register_address: The register address to read the bit from
    :param type bit: The bit index within the byte at ``register_address``
    :param int register_width: The number of bytes in the register. Defaults to 1.

    """

    def __set__(self, obj, value):
        raise AttributeError()
